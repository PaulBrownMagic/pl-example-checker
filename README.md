# Example Checker

`example_checker.pl` will extract example comment blocks from
source code and run them to ensure they are correct.

This is currently only a proof-of-concept.

Run with:

```
   ?- run_examples(sandbox).
```

Or from the commandline:

```
    :~$ swipl -t "run_all_examples(sandbox)" -s example_checker.pl
```

Please view the docs by running `doc_server(8000).`

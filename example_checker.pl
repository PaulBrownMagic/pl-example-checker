#!/usr/bin/env swipl

:- module(example_checker,
    [ run_examples/1
    , run_all_examples/1
    ]
).

/** <module> "Example Checker"

This program will extract examples from
source code documentation and run them to
ensure they are passing.

For compatibility with PLDoc, and so examples
are displayed as code blocks, examples must be
written as:

==
    ```example
    ?- my_predicate(foo, Bar),
    Bar = baz.
    ```
==
**Note**: Currently a syntax error exists in the above, the first line needs to end in a comma

or

==
    ```example
    my_predicate(Bar, baz), Bar = foo.
    ```
==

**Note**: Each example needs to be in its own example block as only
the first term is tested:

==
   ```example
   succ(1, 2).
   ```
   ```example
   succ(2, N), N = 2
   ```
==

This module is designed as a proof-of-concept and for
measuring utility of this feature. It will change! Maybe
you'll change it? Please feel free to improve.

@author Paul Brown
@tbd Fix syntax error in examples! First line should end with a fullstop which we need to replace with a comma

*/

%% run_all_examples(++Source: atom) is semidet.
%
% Non-interactive `run_example/1`
run_all_examples(S) :-
    findall(_, run_examples(S), _), nl.


%! run_examples(++Source:atom) is nondet
% For a given source file step through the
% extracted examples, running them against
% the source.
run_examples(S) :-
    ensure_loaded(S),
    get_example(S, EG, Det),
    ( term_string((?-Test), EG)
    ; \+ term_string((?-_), EG),
      term_string(Test, EG)
    ),
    format(EG),
    call_test(Det, Test).

% Call a test once
call_test(det, Test) :-
    ( call(Test), writeln("Passed")
    ; \+ call(Test), writeln("Failed")
    ).

% Find how many times a test passed
call_test(nondet, Test) :-
    findall(T, (call(Test), write("."), Test=T), Ts),
    length(Ts, X), format(": ~d cases Passed", [X]).


%! get_example(Source, Example, Determinism) is nondet.
% For a given file source Example appears in a comment between
% ```example and ``` fences.
% Determinism is nondet if found in the comment, otherwise det.
get_example(S, Eg, Det) :-
    get_comment(S, StrComm),
    string_codes(StrComm, CodComm),
    (phrase(det(Det), CodComm, _) ; \+ phrase(det(Det), CodComm, _), Det = det),
    phrase(extract_example(EGc), CodComm, _),
    string_codes(Eg, EGc).

%! get_comment(Source, Comment) is nondet.
% True iff Comment is a comment in the
% source file.
get_comment(S, Comment) :-
    xref_source(S),
    xref_comment(S, _, _, Comment) ;
    xref_comment(S, _, Comment).

% See if we can extract nondet from comment,
% otherwise we'll run the example once as if det.
det(nondet) --> chars, "nondet".

% example can be some leading chars, then ```example Code ```
extract_example(Eg) --> chars, open_fence(Style), example(Eg), close_fence(Style).

% Code fences to accept, backticks aren't usually used so no problems
% But the == requires more cleverness
open_fence(bt) --> "```example".
close_fence(bt) --> "```".

% Recursively add characters to example
example([]) --> [].
% Skip comment character
example(T) --> "%", example(T).
% Make sure we don't skip over end of example and disjunction case to not include %
example([H|T]) --> {dif(H, 96), dif(H, 37)}, [H], example(T). % 96 is `, 37 is %

% Skip over characters
chars --> [_].
chars --> [_], chars.

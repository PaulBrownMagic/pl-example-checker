:- module(main,
    [ my_sort/2
    ]
).

%! my_sort(+List, -Sorted) is det
% True if Sorted can be unified ...
%
% ```example
% ?- my_sort([4,3,1,2], Sorted),
% Sorted = [1, 2, 3, 4].
% ```
% Text
%
% ```example
% my_sort([4, 3, 1, 2], [1, 2, 3, 4]).
% ```
my_sort(In, Out) :- sort(In, Out).

%% my_member(X, List) is nondet.
%
%```example
% ?- member(X, [1, 2, 3]),
% X = 1 ;
% X = 2 ;
% X = 3.
%```
my_member(A, L) :- member(A, L).
